��          �            x  j  y     �     �  !        )     F  #   f  /   �  /   �  �   �  �  �  8   �      �       )     �  ?  V  �     !
     8
     L
     g
     �
  "   �
  *   �
  (   �
      �    7   �     �       .   0                                                     	   
                        available update(s) </big></big></b> \n \n <b> 'Automatic' Update </b> - usually it's completely unattended. and normally safe, since it always makes the default choices for you  \n (Pressing the Enter key now will select the Automatic mode) \n <b> 'Manual' Update </b> - gives you full control of the update process, but may require you to make some choices \n 'Automatic' Update 'Manual' Update <b><big><big> There's, at least,  Internet connection detected No Internet connection detected Waiting for a Network connection... You are Root or running the script in sudo mode You entered the wrong password or you cancelled \n <b><big><big>  It seems that apt is already in use.  </big></big></b> \n \n Close any application using it (like Package Installer or Synaptic) and try to run $title again. \n \n (Please note that you can only run one instance of $title at a time) \n \n <b><big><big>  There was an error while updating your system! </big></big></b> \n \n Please read the output in the Terminal window below this one. \n After closing this window, you can try to fix the problem by running <b> sudo apt -f install  </b> \n in a new Terminal window and then try to update your system again. \n \n If the problem still remains, copy any error messages that appear in the Terminal and \n ask for help at antixforum.com \n \n No updates were found \n Your system is up to date \n \n Your system is now updated \n antiX - Updater antiX - Updater: DO NOT CLOSE THIS WINDOW Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-27 12:11+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 tilgjengelig(e) oppdatering(er) </big></big></b> \n \n <b> Automatisk oppdatering</b> – kan kjøres uten å følge med, og er vanligvis trygt fordi standardverdiene brukes \n (Trykk Enter for å velge automatisk modus) \n <b>Manuell oppdatering</b> – gir full kontroll over oppdateringsprosessen, men kan kreve at man tar bevisste valg \n Automatisk oppdatering Manuell oppdatering <b><big><big>Det er minst  Fant internett-tilkobling Fant ingen internett-tilkobling Venter på nettverkstilkobling … Du er root eller kjører skriptet med sudo Du skrev inn feil passord, eller avbrøt \n <b><big><big>  apt kjører allerede.  </big></big></b> \n \n Lukk alle programmer som bruker apt (slik som pakkeinstallasjon eller Synaptic) og forsøkt å kjøre $title igjen. \n \n (Legg merke til at man kun kan kjøre én instans av $title om gangen) \n \n <b><big><big>Det oppstod en feil under oppdatering av systemet!</big></big></b> \n \n Les utdata i terminalvinduet under dette vinduet. \n Lukk dette vinduet og forsøk å fikse problemet ved å kjøre <b>sudo apt -f install</b> \n i et nytt terminalvindu for å forsøke å oppdatere systemet igjen. \n \n Kopier feilmeldinger fra terminalen \n hvis problemet vedvarer og spør om hjelp på antixforum.com \n \n Fant ingen oppdateringer \n Systemet er oppdatert \n \n Systemet er nå oppdatert \n antiX – Oppdateringsverktøy antiX – oppdatering: IKKE LUKK DETTE VINDUET 