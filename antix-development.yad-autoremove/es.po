# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# German Lancheros <glancheros2015@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:51+0200\n"
"PO-Revision-Date: 2021-12-27 14:16+0000\n"
"Last-Translator: German Lancheros <glancheros2015@gmail.com>, 2021\n"
"Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: yad-autoremove:12
msgid "Internet connection detected"
msgstr "Conexión a Internet detectada"

#: yad-autoremove:17
msgid "already root"
msgstr "es root"

#: yad-autoremove:21
msgid "You are Root or running the script in sudo mode"
msgstr "Eres Root o ejecutando el guión en modo sudo"

#: yad-autoremove:23
msgid "You entered the wrong password or you cancelled"
msgstr "Has introducido una contraseña incorrecta o la has cancelado."

#: yad-autoremove:30
msgid "Waiting for a Network connection..."
msgstr "Esperando una conexión de red..."

#: yad-autoremove:30 yad-autoremove:34
msgid "antiX - autoremove"
msgstr "antiX - autoremover"

#: yad-autoremove:39
msgid "No Internet connection detected!"
msgstr "¡No se detecta ninguna conexión a Internet!"
